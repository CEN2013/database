import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;

public class sp2 implements Runnable
{
	public static final String ServerIP = "192.168.1.1";
	public static final int ServerPort = 7788;
	static CommPortIdentifier portId; //串口通信管理类
	@SuppressWarnings("unchecked")
	static Enumeration portList; //已经连接上的端口的枚举
	public static SerialPort serialPort; //串口的引用

	public static OutputStream out;

	public void run()
	{
		try
			{
				System.out.println("Connection...");
				ServerSocket serverSocket = new ServerSocket(ServerPort);
				while (true)
					{
						Socket client = serverSocket.accept();
						System.out.println("Receiveing...");
						try
							{
								BufferedReader bf = new BufferedReader(new InputStreamReader(
										client.getInputStream()));
								String str = bf.readLine();
								if (str.equals("0"))
									{
										out.write(48);
										out.flush();
									}
								else if (str.equals("1"))
									{
										out.write(49);
										out.flush();
									}
								System.out.println("Received:'" + str + "'");

							}
						catch (Exception e)
							{
								e.toString();
							}
						finally
							{
								out.close();
								client.close();
								System.out.println("done");
							}
					}
			}
		catch (Exception e)
			{
				e.toString();
			}
	}

	public static void main(String[] args)
	{

		Thread ServerThread = new Thread(new sp2());
		ServerThread.start();
		try
			{
				portList = CommPortIdentifier.getPortIdentifiers(); //得到当前连接上的端口
				while (portList.hasMoreElements())
					{
						portId = (CommPortIdentifier) portList.nextElement();
						if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL)
							{//判断如果端口类型是串口
								if (portId.getName().equals("COM3"))
									{ //判断如果COM3端口已经启动就连接

										serialPort = (SerialPort) portId.open("COM3", 2000);
										serialPort.setSerialPortParams(38400,
												SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
												SerialPort.PARITY_NONE);
										out = serialPort.getOutputStream();
										System.out.println(portId.getName());
									}
							}
					}
			}
		catch (Exception e)
			{
				e.printStackTrace();
			}
		// TODO Auto-generated method stub
	}
}