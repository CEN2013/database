import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class sql
{
	public static void main(String[] args)
	{
		String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String dbURL = "jdbc:sqlserver://localhost:1433; DatabaseName=student";
		String userName = "sa";
		String userPwd = "password";
		try
			{
				Class.forName(driverName);
				Connection dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
				System.out.println("SQL server connected!");
				Statement stmt = dbConn.createStatement();//create sql object

				System.out.println("Start reading data...\n\n");

				int rowCount = stmt
						.executeUpdate("UPDATE [dbo].[washroom]  SET [airCondition] ='damn good' WHERE level = 1");
				//System.out.println(rowCount);
				ResultSet rs = stmt.executeQuery("SELECT * FROM [dbo].[washroom]");//returns sql result set

				System.out.println("LEVEL\t\t AIR CONDITION");
				System.out.println("------------------------------");
				while (rs.next())//while loop to display rows
					{
						System.out.println(rs.getString("level") + "\t\t"
								+ rs.getString("airCondition"));
					}

				System.out.println("\n\ndata reading completed.\n");

				stmt.close();//关闭命令对象连接

				rs.close();//关闭数据库连接
				System.out.println("SQL server connection closed.");
			}
		catch (Exception e)
			{
				e.printStackTrace();
				System.out.print("Connection failed.");
			}
		//to add in

	}
}